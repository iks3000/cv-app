import './style.scss';

const Button = ({ icon, text="Click" }) => {
    return (
        <button className="button">{icon}<span>{text}</span></button>
    )
}

export default Button;