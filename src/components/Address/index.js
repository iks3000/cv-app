import './style.scss';
import PhoneIcon from '../../assets/images/icons/phone.svg';
import EmailIcon from '../../assets/images/icons/email.svg';
import TwitterIcon from '../../assets/images/icons/twitter.svg';
import FacebookIcon from '../../assets/images/icons/facebook.svg';
import SkypeIcon from '../../assets/images/icons/skype.svg';

const Phone = '500-342-242';
const Email = 'office@kamsolutions.pl';
const Twitter = 'https://twitter.com/wordpress';
const Facebook = 'https://www.facebook.com/facebook';
const Skype = 'kamsolutions.pl';

const Address = () => (
    <div className="address" id="addressComponent">
        <h2 className="address__title">Contacts</h2>
        <div className="address__list">
            <div className="address__item">
                <div className="img-wrapper">
                    <img src={PhoneIcon} alt="phone icon" />
                </div>
                <a className="content-title" href={`tel:+1${Phone}`}>{Phone}</a>
            </div>
            <div className="address__item">
                <div className="img-wrapper">
                    <img src={EmailIcon} alt="email icon" />
                </div>
                <a className="content-title" href={`mailto:${Email}`}>{Email}</a>
            </div>
            <div className="address__item">
                <div className="img-wrapper">
                    <img src={TwitterIcon} alt="twitter icon" />
                </div>
                <div>
                    <h3 className="content-title">Twitter</h3>
                    <a className="content-link" href={Twitter} target="_blank" rel="noreferrer">{Twitter}</a>
                </div>
            </div>
            <div className="address__item">
                <div className="img-wrapper">
                    <img src={FacebookIcon} alt="facebook icon" />
                </div>
                <div>
                    <h3 className="content-title">Facebook</h3>
                    <a className="content-link" href={Facebook} target="_blank" rel="noreferrer">{Facebook}</a>
                </div>
            </div>
            <div className="address__item">
                <div className="img-wrapper">
                    <img src={SkypeIcon} alt="skype icon" />
                </div>
                <div>
                    <h3 className="content-title">Skype</h3>
                    <a className="content-link" href={`callto:${Skype}`}>{Skype}</a>
                </div>
            </div>
        </div>
    </div>
);

export default Address;