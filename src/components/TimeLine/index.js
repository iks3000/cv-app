import { useState } from "react";
import Info from '../../components/Info';
import './style.scss';

const TimeLine = ({ data }) => {
    const [dataSeed] = useState(data);

    return (
        <div className="time-line" id="timeLineComponent">
            <h2 className="time-line__title">Education</h2>
            <ul className="time-line__list">
                {dataSeed.map((dataTimeLine, index) => (
                    <li className="time-line__item" key={index}>
                        <div className="time-line__date">
                            {dataTimeLine.date}
                        </div>
                        <div className="time-line__content">
                            <h3 className="time-line__content-title">
                                {dataTimeLine.title}
                            </h3>
                            <Info text={dataTimeLine.text} />
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    )
};

export default TimeLine;