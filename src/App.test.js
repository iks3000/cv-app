import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Test text', () => {
  render(<App />);
  const textElement = screen.getByText(/Test/i);
  expect(textElement).toBeInTheDocument();
});
